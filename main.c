/************************************************************************//**
 * \brief 1985 Channel
 * \author Jesús Alonso (doragasu)
 * \date   2019
 * \defgroup 1985ch main
 * \{
 ****************************************************************************/
#include <string.h>

#include "vdp.h"
#include "mw/util.h"
#include "mw/mpool.h"
#include "mw/megawifi.h"
#include "mw/loop.h"
#include "mw/gamejolt.h"
#include "credentials.h"

/// Length of the communications buffer
#define MW_BUFLEN	1460

/// Maximum number of loop functions
#define MW_MAX_LOOP_FUNCS	2

/// Maximun number of loop timers
#define MW_MAX_LOOP_TIMERS	4

/// Command buffer
static char cmd_buf[MW_BUFLEN];

static void println(const char *str, int color)
{
	static unsigned int line = 1;

	if (str) {
		VdpDrawText(VDP_PLANEA_ADDR, 2, line, color, 36, str, 0);
	}
	line++;
	if (line > 27) {
		line = 1;
	}
}

static void idle_cb(struct loop_func *f)
{
	UNUSED_PARAM(f);
	mw_process();
}

static enum mw_err assoc(void)
{
	enum mw_err err;

	// Join AP
	println("Associating to AP", VDP_TXT_COL_WHITE);
	err = mw_ap_assoc(0);
	if (err != MW_ERR_NONE) {
		goto out;
	}
	err = mw_ap_assoc_wait(MS_TO_FRAMES(30000));
	if (err != MW_ERR_NONE) {
		goto out;
	}
	println("DONE!", VDP_TXT_COL_CYAN);
	println(NULL, 0);

out:
	return err;
}

static void trophy_print(const struct gj_trophy *trophy)
{
	char line[40];

	if (trophy->secret) {
		println("Secret trophy", VDP_TXT_COL_MAGENTA);
	} else {
		strcpy(line, "title: ");
		strcpy(line + 7, trophy->title);
		println(line, VDP_TXT_COL_WHITE);
	
		strcpy(line, "description: ");
		strcpy(line + 13, trophy->description);
		println(line, VDP_TXT_COL_CYAN);

		strcpy(line, "difficulty: ");
		strcpy(line + 12, gj_trophy_difficulty_str(trophy->difficulty));
		println(line, VDP_TXT_COL_CYAN);

		strcpy(line, "achieved: ");
		strcpy(line + 10, strcmp("false", trophy->achieved) ? trophy->achieved : "No");
		println(line, VDP_TXT_COL_CYAN);
	}
}

static void gj_time_test(void)
{
	struct gj_time time;

	if (!gj_time(&time)) {
		println(time.timestamp, VDP_TXT_COL_WHITE);
		println(time.timezone, VDP_TXT_COL_WHITE);
	} else {
		println("FAILED TO GET TIME", VDP_TXT_COL_MAGENTA);
	}
}

static void gj_trophy_test(void)
{
	char *trophy_data;
	struct gj_trophy trophy;

	if (gj_trophy_remove_achieved("121457")) {
		println("FAILED TO REMOVE ACHIEVEMENT", VDP_TXT_COL_MAGENTA);
	}
	if (gj_trophy_add_achieved("121457")) {
		println("FAILED TO ADD ACHIEVEMENT", VDP_TXT_COL_MAGENTA);
	}
	trophy_data = gj_trophies_fetch(false, NULL);
	if (!trophy_data) {
		println("FAILED TO FETCH TROPHY DATA", VDP_TXT_COL_MAGENTA);
		return;
	}
	while (trophy_data && *trophy_data) {
		trophy_data = gj_trophy_get_next(trophy_data, &trophy);
		if (!trophy_data) {
			println("FAILED TO FORMAT TROPHY", VDP_TXT_COL_MAGENTA);
			return;
		}
		trophy_print(&trophy);
	}
}

static void gj_scores_test(void)
{
	char *score_data;
	struct gj_score_table score_table;
	struct gj_score score;

	if (gj_scores_add("200 patatas", "200", NULL, "Patateitor", "Prueba guest")) {
		println("ADDING SCORE FAILED", VDP_TXT_COL_MAGENTA);
	}
	score_data = gj_scores_tables();
	while (score_data && *score_data) {
		score_data = gj_score_table_get_next(score_data, &score_table);
		if (!score_data) {
			println("FAILED TO FORMAT SCORE", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(score_table.name, VDP_TXT_COL_WHITE);
		println(score_table.description, VDP_TXT_COL_CYAN);
		if (score_table.primary) {
			println("PRIMARY", VDP_TXT_COL_CYAN);
		}
	}
	score_data = gj_scores_fetch(NULL, NULL, NULL, NULL, NULL, false);
	while (score_data && *score_data) {
		score_data = gj_score_get_next(score_data, &score);
		if (!score_data) {
			println("FAILED TO FORMAT SCORE", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(score.user, VDP_TXT_COL_WHITE);
		println(score.score, VDP_TXT_COL_CYAN);
	}
	score_data = gj_scores_get_rank("400", NULL);
	if (score_data) {
		println(score_data, VDP_TXT_COL_WHITE);
	}
}

static void gj_data_store_test(void)
{
	char *data;
	char *value = NULL;

	if (gj_data_store_set("match_user_name", "user_name", false)) {
		println("GLOBAL DATA STORE FAILED", VDP_TXT_COL_MAGENTA);
	}
	if (gj_data_store_set("something_new_again", "new_value_again", true)) {
		println("USER DATA STORE FAILED", VDP_TXT_COL_MAGENTA);
	}

	if (!(data = gj_data_store_keys_fetch(NULL, false))) {
		println("DATA STORE KEYS FETCH FAILED", VDP_TXT_COL_MAGENTA);
	}
	while (data && *data) {
		data = gj_data_store_key_next(data, &value);
		if (!data) {
			println("FAILED TO FORMAT SCORE", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(value, VDP_TXT_COL_WHITE);
	}

	if (!(data = gj_data_store_keys_fetch(NULL, true))) {
		println("DATA STORE KEYS FETCH FAILED", VDP_TXT_COL_MAGENTA);
	}
	while (data && *data) {
		data = gj_data_store_key_next(data, &value);
		if (!data) {
			println("FAILED TO FORMAT SCORE", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(value, VDP_TXT_COL_WHITE);
	}

	if (!(data = gj_data_store_keys_fetch("match*", false))) {
		println("DATA STORE KEYS FETCH FAILED", VDP_TXT_COL_MAGENTA);
	}
	while (data && *data) {
		data = gj_data_store_key_next(data, &value);
		if (!data) {
			println("FAILED TO FORMAT KEYS", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(value, VDP_TXT_COL_WHITE);
	}

	if (!(data = gj_data_store_keys_fetch("*again", true))) {
		println("DATA STORE KEYS FETCH FAILED", VDP_TXT_COL_MAGENTA);
	}
	while (data && *data) {
		data = gj_data_store_key_next(data, &value);
		if (!data) {
			println("FAILED TO FORMAT KEYS", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(value, VDP_TXT_COL_WHITE);
	}

	data = data_store_fetch("something_new", true);
	if (data) {
		println(data, VDP_TXT_COL_WHITE);
	} else {
		println("DATA STORE FETCH FAILED", VDP_TXT_COL_MAGENTA);
	}

	if (!gj_data_store_update("something_new", GJ_OP_APPEND,
				"_updated", true)) {
		println("DATA STORE UPDATE FAILED", VDP_TXT_COL_MAGENTA);
	}
	if (gj_data_store_remove("something_new", true)) {
		println("DATA STORE REMOVE FAILED", VDP_TXT_COL_MAGENTA);
	}
}

static void gj_sessions_test(void)
{
	if (gj_sessions_open()) {
		println("SESSION OPEN FAILED", VDP_TXT_COL_MAGENTA);
		return;
	}
	if (gj_sessions_ping(true)) {
		println("SESSION PING TRUE FAILED", VDP_TXT_COL_MAGENTA);
		return;
	}
	mw_sleep(MS_TO_FRAMES(20000));
	if (gj_sessions_ping(false)) {
		println("SESSION PING FALSE FAILED", VDP_TXT_COL_MAGENTA);
		return;
	}
	mw_sleep(MS_TO_FRAMES(5000));
	if (gj_sessions_check(NULL, NULL)) {
		println("USER IS ACTIVE", VDP_TXT_COL_WHITE);
	}
	if (gj_sessions_close()) {
		println("SESSIONS CLOSE FAILED", VDP_TXT_COL_MAGENTA);
	}
}

static void gj_users_test(void)
{
	struct gj_user user;
	char *data, *friend;

	if (!gj_users_auth()) {
		println("USER AUTH FAILED", VDP_TXT_COL_MAGENTA);
		return;
	}

	if (!(data = gj_users_fetch("doragasu", NULL))) {
		println("USER FETCH FAILED", VDP_TXT_COL_WHITE);
		return;
	}
	while (data && *data) {
		data = gj_user_get_next(data, &user);
		if (!data) {
			println("FAILED TO FORMAT USER", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(user.id, VDP_TXT_COL_WHITE);
		println(user.username, VDP_TXT_COL_WHITE);
		println(user.status, VDP_TXT_COL_WHITE);
		println(user.type, VDP_TXT_COL_WHITE);
		println(user.last_logged_in, VDP_TXT_COL_WHITE);
		println(user.developer_name, VDP_TXT_COL_WHITE);
	}
	if (!(data = gj_friends_fetch())) {
		println("FRIENDS FETCH FAILED", VDP_TXT_COL_MAGENTA);
		return;
	}
	while (data && *data) {
		data = gj_friend_get_next(data, &friend);
		if (!data || !friend) {
			println("FRIENDS LIST FAILED", VDP_TXT_COL_MAGENTA);
			return;
		}
		println(friend, VDP_TXT_COL_WHITE);
	}
}

static void gj_test(void)
{
	if (gj_init("https://api.gamejolt.com/api/game/v1_2/", GJ_GAME_ID,
				GJ_PRIV_KEY, GJ_USERNAME, GJ_USER_TOKEN,
				cmd_buf, MW_BUFLEN, MS_TO_FRAMES(30000))) {
		println("API INIT ERROR", VDP_TXT_COL_MAGENTA);
		return;
	}

//	gj_time_test();
	gj_trophy_test();
//	gj_scores_test();
//	gj_data_store_test();
//	gj_sessions_test();
//	gj_users_test();
}

static void run_test(struct loop_timer *t)
{
	if (assoc()) {
		goto err;
	}

	gj_test();

	goto out;

err:
	println("ERROR!", VDP_TXT_COL_MAGENTA);
	mw_ap_disassoc();

out:
	mw_power_off();
	loop_timer_del(t);
}

/// MegaWiFi initialization
static void megawifi_init_cb(struct loop_timer  *t)
{
	uint8_t ver_major = 0, ver_minor = 0;
	char *variant = NULL;
	enum mw_err err;
	char line[] = "MegaWiFi version X.Y";

	// Try detecting the module
	err = mw_detect(&ver_major, &ver_minor, &variant);

	if (MW_ERR_NONE != err) {
		// Megawifi not found
		println("MegaWiFi not found!", VDP_TXT_COL_MAGENTA);
	} else {
		// Megawifi found
		line[17] = ver_major + '0';
		line[19] = ver_minor + '0';
		println(line, VDP_TXT_COL_WHITE);
		println(NULL, 0);
		// Configuration complete, run test function next frame
		t->timer_cb = run_test;
		loop_timer_start(t, 1);

	}
}

/// Loop run while idle
static void main_loop_init(void)
{
	// Run next frame, do not auto-reload
	static struct loop_timer frame_timer = {
		.timer_cb = megawifi_init_cb,
		.frames = 1
	};
	static struct loop_func megawifi_loop = {
		.func_cb = idle_cb
	};

	loop_init(MW_MAX_LOOP_FUNCS, MW_MAX_LOOP_TIMERS);
	loop_timer_add(&frame_timer);
	loop_func_add(&megawifi_loop);
}

/// Global initialization
static void init(void)
{
	// Initialize memory pool
	mp_init(0);
	// Initialize VDP
	VdpInit();
	// Initialize game loop
	main_loop_init();
	// Initialize MegaWiFi
	mw_init(cmd_buf, MW_BUFLEN);
}

/// Entry point
int main(void)
{
	init();

	// Enter game loop (should never return)
	loop();

	return 0;
}

/** \} */

