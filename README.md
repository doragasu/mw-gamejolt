# mw-gamejolt

## Introduction

This repository contains an usage example of the Gamejolt Game API implementation for MegaWiFi. The sources contain several test functions to:

* Get date/time from GameJolt servers.
* Get trophies, add/remove trophy achievements.
* Get score tables, add scores.
* Add data to storage, operate on data, retrieve data.
* Open and update session.
* Fetch user, fetch user friends.

By default only the trophies example function is built, the other ones are commented out. Uncomment the functions you want to test and build the example.

To run the test, you will need a MegaWiFi cart or emulator with MegaWiFi API support 1.4 or later.

## Building

To build the code, you need a `m68k-elf` toolchain with `newlib` support You can use [my docker-deb-m68k Docker images](https://gitlab.com/doragasu/docker-deb-m68k) to get one.

Before building, you must create in the root folder a `credentials.h` file with the user and game credentials. You must create an user account and a game profile on GameJolt to get them. In the `credentials.h` file you must put the following:

```c
#ifndef _CREDENTIALS_H_
#define _CREDENTIALS_H_

#define GJ_GAME_ID	"123456"
#define GJ_PRIV_KEY	"1234567890abcdef1234567890abcdef"

#define GJ_USERNAME	"user_name"
#define GJ_USER_TOKEN	"xXxXxX"

#endif /*_CREDENTIALS_H_*/
```
Replace all the defined constants with their correct values. The game related parameters can be obtained by entering GameJolt web, going to the game management page, then entering *Game API*, and *API Settings*. There you will have both *Game ID* and *Private Key* to enter in `GJ_GAME_ID` and `GJ_PRIV_KEY`. For the user parameters, `GJ_USERNAME` is just the GameJolt user name, and to get the `GJ_USER_TOKEN`, the user has to go to enter the account in the GameJolt web, then pop the user menu and select *Game Token*.

Once the `credentials.h` file has been properly populated, just call `make` to build the sources.
